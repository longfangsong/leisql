# How to use

## Create table

```sh
curl --request POST \
  --url http://127.0.0.1:3000/view \
  --data '{
  "sql": "create table t(a int, b int);"
}'
```

## Create view

```sh
curl --request POST \
  --url http://127.0.0.1:3000/view \
  --data '{
  "sql": "select a from t;"
}'
```

## Push data into table

```sh
curl --request POST \
  --url http://127.0.0.1:3000/view \
  --data '{
  "sql": "insert into t (a, b) values (1, 2);"
}'
```

## Poll all data in the stream after last poll

Or always return `[]` if you unsubscribed.

```sh
curl --request GET \
  --url 'http://127.0.0.1:3000/view?id=0'
```

## Delete the view

```sh
curl --request DELETE \
  --url 'http://127.0.0.1:3000/view?id=0'
```
